(function(){
	$('#projects').on('click','.delete-project',function(){
		$(this).closest('.app').fadeOut();
	});

	$('#projects').on('click','.open-menu',function(){
		$(this).closest('.app').find('.submenu').toggleClass('opened');
	});

	$('#projects').on('click','.new-project-btn',function(){
		var name_project = prompt('Choose a name for the new project');
		var creation_time = new Date().getDate() + '/' + (new Date().getMonth()+1) + '/' + new Date().getFullYear() + ' ' + new Date().getHours() + ':' + new Date().getMinutes();
		var new_project_script = '<div class="app"><div class="submenu"><ul><li>Download App</li><li>Clone</li><li>Share</li></ul></div><img src="img/default_project_icon.svg" class="project_icon" alt="default project icon"><div class="name-project">' + name_project + '</div><div class="time-update">' + creation_time + '</div><div class="delete-project">X</div><div class="buttons-down"><div class="barretta"></div><ul class="build-icons"><li><i class="fa fa-pencil"></i></li><li><i class="fa fa-apple"></i></li><li><i class="fa fa-android"></i></li><li class="open-menu"><i class="fa fa-bars"></i></li></ul></div></div>';
		$('#new-app').after(new_project_script);
	});

})();