(function(){

	var json_arr;
	var c = 1;
	var selection_type;
	var index_cell;
	var num_cell;
	var working;

	//Lettura domande e tavola da file JSON
	$.getJSON('http://redeemer87.bitbucket.org/Parole_Crociate_2/lista_parole.json', function(data){
		

		json_arr = eval('(' + JSON.stringify(data) + ')');

		//Scrittura tavola 21x12
		$('<table>').attr('id', 'board').appendTo('body');
		$('<tr>').appendTo('#board');

		for (var i=0; i<json_arr.board.length;i++){

			if( json_arr.board[i] == 1){
				$('<td>').appendTo('#board tr:last');
				$('<div/>').attr('class', 'num-quest').html(c).appendTo('#board td:last');
				$('<input>').attr({'type':'text', 'class': 'input-text', 'maxlength': '1'}).appendTo('#board td:last');
				c++;
			}else if( json_arr.board[i] == 2){
				$('<td>').appendTo('#board tr:last');
				$('<input>').attr({'type':'text', 'class': 'input-text', 'maxlength': '1'}).appendTo('#board td:last');
			}else{
				$('<td/>').attr('class', 'disabled').appendTo('#board tr:last');
			}

			if( (((i+1) % 21) == 0) && (i>0) ) {
				$('<tr>').appendTo('#board');
			}
		}



		//Scrittura domande orizzontali e verticali
		$('<div/>').attr('id', 'questions').appendTo('body');
		$('<div/>').attr('id', 'horizontal').appendTo('#questions');
		$('<div/>').attr('id', 'vertical').appendTo('#questions');		
		$('<b>Orizzontali</b>').appendTo('#horizontal');
		$('<b>Verticali</b>').appendTo('#vertical');
		
		for (var i=0; i<json_arr.parole.length;i++){
			if( json_arr.parole[i].direzione === 'oriz'){
				$('<p>').html('<b>' + json_arr.parole[i].n_definizione + '. </b>' + json_arr.parole[i].definizione).appendTo('#horizontal');
			}else{
				$('<p>').html('<b>' + json_arr.parole[i].n_definizione + '. </b>' + json_arr.parole[i].definizione).appendTo('#vertical');
			}
		}

	});

	//Seleziona n_definizione
	$('body').on('click', 'td', function(){
		selection_type = '';
		working = true;

		index_cell = $('td').index(this);	

		//Se la n_definizione non è disabilitata la evidenzia
		if($(this).hasClass('disabled') == false){
			$('td').removeClass('word-checking');
			$(this).addClass('word-checking');
		}

		//Controlla se la n_definizione cliccata ha un numero corrispondente ad una domanda
		if($(this).find('div').length > 0) {
			$('td').removeClass('word-checking');
			num_cell = $(this).find('div').text();

			//Evidenzia le celle successive a quella cliccata
			for (var i=0; i<json_arr.parole.length;i++){
				if(json_arr.parole[i].n_definizione == num_cell){

					//Controlla se le celle successive sono orizzontali o verticali
					if( json_arr.parole[i].direzione == 'oriz' ){
						selection_type = 'oriz';
						for(var x=0;x<json_arr.parole[i].soluzione.length;x++){
							$('td').eq(index_cell+x).addClass('word-checking');
						}
					}else if( (json_arr.parole[i].direzione == 'vert')  && (selection_type != 'oriz') ){
						selection_type = 'vert';
						for(var x=0;x<json_arr.parole[i].soluzione.length;x++){
							$('td').eq(index_cell+(x*21)).addClass('word-checking');
						}
					}
				}
			}

		}else{

		}

	});

	//Cambiare con un DOPPIO CLICK da soluzione verticale a orizzontale e viceversa
	$('body').on('dblclick', 'td', function(){
		index_cell = $('td').index(this);

		//Controlla se la n_definizione cliccata ha un numero corrispondente ad una domanda
		if($(this).find('div').length > 0) {
			num_cell = $(this).find('div').text();

			//Evidenzia le celle successive a quella cliccata
			for (var i=0; i<json_arr.parole.length;i++){
				if(json_arr.parole[i].n_definizione == num_cell){

					//Controlla se le celle successive sono orizzontali o verticali
					if( (selection_type == 'oriz') && (json_arr.parole[i].direzione != 'oriz') ){
						selection_type = 'vert';
						$('td').removeClass('word-checking');
						for(var x=0;x<json_arr.parole[i].soluzione.length;x++){
							$('td').eq(index_cell+(21*x)).addClass('word-checking');
						}
					}else if( (selection_type == 'vert') && (json_arr.parole[i].direzione != 'vert') ) {
						selection_type = 'oriz';
						$('td').removeClass('word-checking');
						for(var x=0;x<json_arr.parole[i].soluzione.length;x++){
							$('td').eq(index_cell+x).addClass('word-checking');
						}
					}
				}
			}
		}
	});

	//Deselezionare tutte le celle
	$('body').on('focusout', 'td', function(){
			working = false;
	});

	//Gestione della pressione dei tasti
	$('body').on('keydown', 'td', function(e){
		var keyCode = e.keyCode || e.which;	

		//Cambiare con il tasto ALT da soluzione verticale a orizzontale e viceversa
		if(keyCode == 18){

			if(working) {

				if(selection_type == 'oriz'){
					num_cell = $('td').eq(index_cell).find('div').text();
					
					selection_type = 'vert';
					for (var i=0; i<json_arr.parole.length;i++){
						if((json_arr.parole[i].n_definizione == num_cell)  && (json_arr.parole[i].direzione != 'oriz')){
							$('td').removeClass('word-checking');
							for(var x=0;x<json_arr.parole[i].soluzione.length;x++){
								$('td').eq(index_cell+(21*x)).addClass('word-checking');
							}
						}
					}
				}else if(selection_type == 'vert'){
					num_cell = $('td').eq(index_cell).find('div').text();
					selection_type = 'oriz';
					for (var i=0; i<json_arr.parole.length;i++){
						if( (json_arr.parole[i].n_definizione == num_cell) && (json_arr.parole[i].direzione != 'vert') ){
							$('td').removeClass('word-checking');
							for(var x=0;x<json_arr.parole[i].soluzione.length;x++){
								$('td').eq(index_cell+x).addClass('word-checking');
							}
						}
					}
				}
			}
		
		}
	});

	//Controllo delle parole inserite
	$('body').on('click','button', function(){
		var temp_c = 0;
		var word_to_check;
		var word_correct = false;

		$('td').removeClass('word-checking');


		// for che fa riferimento alla tavola
		for (var i = 0; i < json_arr.board.length; i++) {
			
			num_cell = $('td').eq(i).find('div').text();

			//for che fa riferimento alla lista di parole
			for (var y = 0; y < json_arr.parole.length; y++) {

				if( json_arr.parole[y].n_definizione == num_cell){

					// controllo se la soluzione è in orizzontale
					if( json_arr.parole[y].direzione == 'oriz' ){
						temp_c = 0;

						//for che fa riferimento al numero delle lettera della soluzione
						for (var x = 0; x < json_arr.parole[y].soluzione.length; x++) {
							
							//controllo se la soluzione è già stata verificata
							if(!$('td').eq(i+x).hasClass('word_correct')){
								word_to_check = '' + $('td').eq(i+x).find('input').val();

								//controllo lettera immessa con quella della soluzione corrispondente
								if(word_to_check.toUpperCase() == json_arr.parole[y].soluzione.charAt(x)){
									temp_c++;
								}else if(word_to_check == ''){
									x = json_arr.parole[y].soluzione.length;
								}
							}else{
								x = json_arr.parole[y].soluzione.length;
							}

						}

						//assegnazione del colore verde alle celle della soluzione
						if(temp_c == json_arr.parole[y].soluzione.length){
							for (var x = 0; x < json_arr.parole[y].soluzione.length; x++) {
								if(!$('td').eq(i+x).hasClass('word-correct')){
									$('td').eq(i+x).addClass('word-correct');
								}
								$('td').eq(i+x).find('input').attr('readonly',true);
							}
						}
					// controllo se la soluzione è in verticale
					}else if( json_arr.parole[y].direzione == 'vert' ){

						temp_c = 0;

						//for che fa riferimento al numero delle lettera della soluzione
						for (var x = 0; x < json_arr.parole[y].soluzione.length; x++) {

							//controllo se la soluzione è già stata verificata
							if(!$('td').eq(i+x).hasClass('word_correct')){
								word_to_check = '' + $('td').eq(i+(x*21)).find('input').val();

								//controllo lettera immessa con quella della soluzione corrispondente
								if(word_to_check.toUpperCase() == json_arr.parole[y].soluzione.charAt(x)){
									temp_c++;
								}else if(word_to_check == ''){
									x = json_arr.parole[y].soluzione.length;
								}
							}else{
									x = json_arr.parole[y].soluzione.length;
							}

						}

						//assegnazione del colore verde alle celle della soluzione
						if(temp_c == json_arr.parole[y].soluzione.length){
							for (var x = 0; x < json_arr.parole[y].soluzione.length; x++) {			
								if(!$('td').eq(i+(x*21)).hasClass('word-correct')){
									$('td').eq(i+(x*21)).addClass('word-correct');
								}else{
								}
								$('td').eq(i+(x*21)).find('input').attr('readonly',true);
							}
						}

					}

				}
				
			}

			
			
		}
	});

})();