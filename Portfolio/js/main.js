(function(){

	$("#work-btn").on('click', function() {
    	$('html, body').animate({scrollTop: $("#work").offset().top}, 500);
	});

	$("#about-btn").on('click', function() {
    	$('html, body').animate({scrollTop: $("#about").offset().top}, 500);
	});

	$("#more-projects").on('click','#more-projects-btn', function() {
    	var numProjects = $("#more-projects").find('> div').length;
    	for (var i = 0; i <numProjects; i++) {
    		if($("#more-projects").find('> div').eq(i).hasClass('not-visible')){
    			$("#more-projects").find('> div').eq(i).removeClass('not-visible');
    		}
    	}
    	$('#more-projects-btn').addClass('not-visible');
	});

})();