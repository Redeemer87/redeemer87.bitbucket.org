(function(){
	$('button').on('click', function(){
		var value = $(this).text();
		value++;
		var property = $(this).prop('class')

		$(this).text(value);
		
		switch(property){
			case 'zero':
				$(this).removeClass('zero');
				$(this).addClass('one');
				break;
			case ('one'):
				$(this).removeClass('one');
				$(this).addClass('two');
				break;
			case 'two':
				$(this).removeClass('two');
				$(this).addClass('three');
				break;
			case 'three':
				$(this).removeClass('three');
				$(this).addClass('zero');
				break;			
		}
	});
})();