(function(){
	$('.add').on('click', function(){
		var newFieldText = $('.new-field').val();

		if(newFieldText.length>0){
			$('.to-do-list').append('<li contenteditable="true">' + newFieldText + '</li>');
			$('.new-field').val('');
		}else{
			alert('please insert a valid text');
		}
	});

})();